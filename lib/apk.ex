defmodule Buildah.Apk.Sqlite do

    alias Buildah.{Apk, Apk.Catatonit, Cmd}

    def on_container(container, options) do
        Apk.packages_no_cache(container, [
            "sqlite"
        ], options)
        {_, 0} = Cmd.run(container, ["sh", "-c", "command -v sqlite3"])
    end

    def test(container, image_ID, options) do
        {_, 0} = Cmd.run(container, ["sh", "-c", "command -v sqlite3"], into: IO.stream(:stdio, :line))
        {_, 0} = Podman.Cmd.run(image_ID, ["sh", "-c", "command -v sqlite3"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
    end

end

